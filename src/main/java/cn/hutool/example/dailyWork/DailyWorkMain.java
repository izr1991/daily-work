package cn.hutool.example.dailyWork;

import cn.hutool.cron.CronUtil;

/**
 * 定时任务主程序，通过此类的Main方法启动定时任务，定时任务对应配置文件：config/cron.setting
 * 
 * @author Looly
 *
 */
public class DailyWorkMain {
	public static void main(String[] args) {
		// 设置秒匹配（只有在定时任务精确到秒的时候使用）
		CronUtil.setMatchSecond(true);
		
		// 启动定时任务，自动加载配置文件中的内容
		CronUtil.start();
	}
}
