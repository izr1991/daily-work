package cn.hutool.example.dailyWork;

import java.io.File;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.cron.task.Task;
import cn.hutool.extra.mail.MailUtil;
import cn.hutool.log.StaticLog;

/**
 * 邮件发送任务<br>
 * 使用Hutool的定时任务发送邮件，正文是生成的日报
 * 
 * @author Looly
 *
 */
public class MailSendTask implements Task{

	@Override
	public void execute() {
		// 今天的日期，格式类似：2019-06-20
		String today = DateUtil.today();
		
		// 生成汇报Word
		File dailyWorkDoc = DailyWorkGenerator.generate();
		// 发送邮件
		MailUtil.sendHtml("hutool@foxmail.com", StrUtil.format("{} 工作日报", today), "请见附件。", dailyWorkDoc);
		
		StaticLog.debug("{} 工作日报已发送给领导！", today);
	}
}
